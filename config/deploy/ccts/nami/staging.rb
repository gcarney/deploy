
set :rails_env, "staging"

set :user, "nami"
set :repository, "git@bitbucket.org:gcarney/nami.git"

set :deploy_to,   "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

role :app, "nami.staging.cctsbaltimore.org"
role :web, "nami.staging.cctsbaltimore.org"
role :db,  "nami.staging.cctsbaltimore.org", :primary => true

