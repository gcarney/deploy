
set :rails_env, "production"

set :user, "pgcasa"
set :repository, "git@bitbucket.org:gcarney/pgcasa.git"

set :deploy_to,   "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

role :app, "66.175.213.225"
role :web, "66.175.213.225"
role :db,  "66.175.213.225", :primary => true

# custom deploy tasks
namespace :deploy do

  desc "Set file and directory permissions."
  task :compile_assets, :roles => [:web, :app] do
  end

end

