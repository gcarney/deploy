
  # shared configuration for all clients.

before 'deploy:setup', 'rvm:install_rvm'
before 'deploy:setup', 'rvm:install_ruby', 'deploy:set_default_ruby'
after  'deploy:setup', 'deploy:install_gems'
after  'deploy:setup', 'deploy:make_special_dirs'
after  'deploy:setup', 'deploy:make_unicorn_socket_dir'
after  'deploy:setup', 'deploy:make_assets_dir'

after "deploy:update_code", "deploy:update_config"
after "deploy:update_config", 'deploy:compile_assets',"deploy:set_file_permissions"
after "deploy:finalize_update", 'deploy:run_bundler'

set :rvm_ruby_string, 'ruby-1.9.3-p125'

set :application, "pennstation"

set :group, "webapp"
set :web_server_group, "nginx"
set :use_sudo, false

set :scm, :git

set :branch do
  default_branch = 'master'
  branch = Capistrano::CLI.ui.ask "SCM branch to deploy: [#{default_branch}] "
  branch = default_branch if branch.empty?
  branch
end

set :deploy_via, :copy
set :copy_dir, '/opt/tmp'
set :copy_compression, :gzip
set :copy_exclude, %w(.git .gitignore features docs spec)

namespace :deploy do

  task :make_unicorn_socket_dir, :roles => [:app] do
    run <<-CMD
      mkdir -p #{shared_path}/tmp
    CMD
  end

  task :make_special_dirs, :roles => [:app] do
    run <<-CMD
      mkdir -p #{deploy_to} &&
      mkdir -p #{copy_remote_dir}
    CMD
  end

  task :make_assets_dir, :roles => [:app] do
    run <<-CMD
      mkdir -p #{shared_path}/assets #{shared_path}/assets/uploaded_images #{shared_path}/assets/uploaded_files
    CMD
  end

  task :set_default_ruby, :roles => [:app] do
    #run "rvm alias create default ruby-1.9.3-p125"
    run "rvm alias create default #{rvm_ruby_string}"
  end

  desc "Install ruby gems."
  task :install_gems, :roles => [:app] do
    # This is here for PennStation2 clients to override.
  end

  desc "Commands to run after code deploy."
  task :update_config, :roles => [:web, :app] do

    config_files = []
    config_files << "database.yml"
    config_files << "unicorn.rb"

    config_files.each do |cf|
      run "/bin/cp #{user_config_dir}/#{cf}.#{rails_env} #{release_path}/config/#{cf}"
    end
  
    initializers = []
  
    initializers.each do |cf|
      run "/bin/cp #{user_config_dir}/#{cf}.#{rails_env} #{release_path}/config/initializers/#{cf}"
    end
  end

  task :finalize_update, :except => { :no_release => true }, :roles => [:web, :app] do
    run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)

    run <<-CMD
      mkdir -p #{latest_release}/tmp &&
      ln -s #{shared_path}/log #{latest_release}/log &&
      ln -s #{shared_path}/pids #{latest_release}/tmp/pids  &&
      ln -s #{shared_path}/ruby #{latest_release}/vendor/ruby &&
      set -e &&
      for d in `ls #{shared_path}/assets`; do
        ln -s #{shared_path}/assets/$d #{latest_release}/public/$d; done
    CMD
  end

  desc "Run bundle command."
  task :run_bundler, :except => { :no_release => true }, :roles => [:app] do
    run "(. /home/#{user}/.bashrc; cd #{latest_release}; bundle install --path=#{shared_path} --deployment --local --binstubs)"
  end

  desc "Compile Rails assets."
  task :compile_assets, :roles => [:web, :app] do
    run "ln -s #{shared_path}/assets #{latest_release}/public/assets"
    #run "(cd #{latest_release}; bundle exec rake assets:precompile)"
    run "(cd #{latest_release}; bundle exec rake assets:precompile:primary RAILS_ENV=#{rails_env} RAILS_GROUPS=assets; bundle exec rake assets:precompile:nondigest RAILS_ENV=#{rails_env} RAILS_GROUPS=assets)"
  end

  desc "Set file and directory permissions."
  task :set_file_permissions, :roles => [:web, :app] do

      # set file and directory ownership.

    run "/bin/chown -R #{user}:#{group} #{release_path}"

      # set file and directory permissions.

    run "/bin/find #{release_path} #{deploy_to}/shared -type d -exec /bin/chmod 0700 {} \\;"
    run "/bin/find #{release_path} #{deploy_to}/shared ! -type d -a ! -type l -exec /bin/chmod 0600 {} \\;"

    run "/bin/mkdir -p #{release_path}/utils #{release_path}/script #{release_path}/bin"
    run "/bin/chmod -R 0700 #{release_path}/utils #{release_path}/script #{release_path}/bin"

      # set file and directory permissions so web server user can access static assets.
      #
      # NOTE: permissions on directories above release_path must be set manually.
      #       this should be a one-time operation during initial app setup.

    run "/bin/chmod 0750 #{release_path}"
    run "/bin/chgrp #{web_server_group} #{release_path}"

    run "/bin/find #{release_path}/public -type d -exec /bin/chmod 0750 {} \\;"
    run "/bin/find #{release_path}/public ! -type d -a ! -type l -exec /bin/chmod 0640 {} \\;"
    run "/bin/chgrp -R #{web_server_group} #{release_path}/public"

      # this directory holds the maintenance page.

    run "/bin/chmod 0750 #{deploy_to}/shared #{deploy_to}/releases"
    run "/bin/chgrp #{web_server_group} #{deploy_to}/shared #{deploy_to}/releases"

    run "/bin/find #{deploy_to}/shared/system -type d -exec /bin/chmod 0750 {} \\;"
    run "/bin/find #{deploy_to}/shared/system ! -type d -a ! -type l -exec /bin/chmod 0640 {} \\;"

    run "/bin/chgrp -R #{web_server_group} #{deploy_to}/shared/system"
    run "/bin/chmod -R 0770 #{deploy_to}/shared/system"

    run "/bin/chgrp -R #{web_server_group} #{deploy_to}/shared/tmp"
    run "/bin/chmod -R 0770 #{deploy_to}/shared/tmp"

    run "/bin/chgrp -R #{web_server_group} #{deploy_to}/shared/assets"
    run "/bin/chmod -R 0770 #{deploy_to}/shared/assets"
  end

  desc "Start app servers"
  task :start, :roles => :app do
    begin
      run "(. /home/#{user}/.bashrc; #{latest_release}/utils/start_app_servers)"
    rescue RuntimeError => e 
      puts e
      puts "There was an error starting the app servers."
    end
  end

  desc "Stop app servers"
  task :stop, :roles => :app do
    begin
      run "(. /home/#{user}/.bashrc; #{latest_release}/utils/stop_app_servers)"
    rescue RuntimeError => e 
      puts e
      puts "There was an error stopping the app servers."
    end
  end

  desc "Restart app servers"
  task :restart, :roles => :app do
    begin
      run "(. /home/#{user}/.bashrc; #{latest_release}/utils/restart_app_servers)"
    rescue RuntimeError => e 
      puts e
      puts "There was an error restarting the app servers."
    end
  end
end
